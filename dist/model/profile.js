"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import mongoose, { Schema } from 'mongoose'
var mongoose = require('mongoose');
exports.ProfileSchema = new mongoose.Schema({
    email: {
        type: String,
        lowercase: true,
        trim: true,
        index: true,
        unique: true,
        required: true,
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    },
    password: {
        type: String,
        required: true
    },
    username: {
        type: String,
        trim: true,
        required: true
    }
}, { collection: 'profiles' });
exports.default = exports = exports = mongoose.model('Profile', exports.ProfileSchema);
//# sourceMappingURL=profile.js.map