"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var bcrypt = require('bcryptjs');
const profile_1 = require("../../model/profile");
function newprofile(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const SALT_WORK_FACTOR = 10;
        const { email, password, confirmPassword, username } = req.body;
        profile_1.default.findOne({
            'email': email
        }).then((user) => __awaiter(this, void 0, void 0, function* () {
            if (!user) {
                const chkEmail = email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) !== null ? true : false;
                const chkPassword = password.length >= 8 && password.length < 20 && password === confirmPassword ? true : false;
                const chkUsername = username.length >= 4 && username.length < 20 && username.match(/^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/) !== null ? true : false;
                if (chkEmail && chkPassword && chkUsername) {
                    const salt = yield bcrypt.genSalt(SALT_WORK_FACTOR);
                    const hashPassword = yield bcrypt.hash(password, salt);
                    var newProfile = new profile_1.default({
                        email: email,
                        password: hashPassword,
                        username: username
                    });
                    newProfile.save().then(savedProfile => {
                        console.log(savedProfile);
                        res.send({ message: 'Your new profile has been registered successfully.' });
                    }).catch(save_err => {
                        console.log('profile save error: ', save_err);
                    });
                }
            }
            else {
                res.send({ message: 'User with this email has registered already.' });
            }
        }));
        //res.send(' I got it, bitch ');
    });
}
exports.newprofile = newprofile;
//# sourceMappingURL=newprofile.js.map