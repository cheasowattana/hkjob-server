"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const profile_1 = require("../../model/profile");
function login(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const loginInfo = req.body;
        profile_1.default.findOne({
            email: loginInfo.email
        }).then(userInfo => {
            //console.log( 'userInfo', userInfo )
            if (userInfo) {
                bcrypt.compare(loginInfo.password, userInfo.password, function (err, result) {
                    if (err) {
                        console.log('bcrypt compare error', err);
                    }
                    if (result) {
                        const payload = {
                            iss: 'http://localhost:4000',
                            email: userInfo.email,
                            username: userInfo.username
                        };
                        const token = jwt.sign(payload, 'devel-jd', { expiresIn: 60 * 120 });
                        res.json({ success: true, message: 'Login Successful', token: token });
                    }
                    else {
                        res.json({ success: false, message: 'User/password is not matched' });
                    }
                });
            }
            else {
                res.json({ success: false, message: 'User/password is not matched' });
            }
        }).catch(err => {
            console.log(err);
        });
    });
}
exports.login = login;
//# sourceMappingURL=login.js.map