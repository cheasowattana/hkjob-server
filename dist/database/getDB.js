"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var uriString = process.MONOGO_URI || 'mongodb://localhost/hkjob';
exports.default = () => {
    mongoose.connect(uriString).then(res => {
        console.log('DB Connected');
    }).catch(err => {
        console.log('DB Error: ', err);
    });
};
//# sourceMappingURL=getDB.js.map