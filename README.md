First, install the npm packages for the project

        # yarn install

Then, you can start the project with:

        # yarn start

Make sure you have the monogoDB service running on port 27017 as well.