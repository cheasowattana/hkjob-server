var mongoose = require('mongoose')

var uriString = process.MONOGO_URI || 'mongodb://localhost/hkjob'

export default () => {
    mongoose.connect( uriString ).then ( res => {
        console.log('DB Connected')
    } ).catch( err => {
        console.log( 'DB Error: ', err )
    } );
}