var bcrypt = require('bcryptjs')
import { Request, Response, NextFunction } from 'express'
import Profile from '../../model/profile'

export async function newprofile( req: Request, res: Response ){
    const SALT_WORK_FACTOR = 10;
    const { email, password, confirmPassword, username } = req.body;

    Profile.findOne( {
        'email': email
    } ).then( async user => {

        if( !user ){

            const chkEmail = email.match( /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ) !== null ? true : false;
            const chkPassword = password.length >= 8 && password.length < 20 && password === confirmPassword ? true : false;
            const chkUsername = username.length >= 4 && username.length < 20 && username.match( /^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/ ) !== null ? true : false;

            if( chkEmail && chkPassword && chkUsername ){
                const salt = await bcrypt.genSalt( SALT_WORK_FACTOR );
                const hashPassword = await bcrypt.hash( password, salt );

                var newProfile = new Profile( {
                    email: email,
                    password: hashPassword,
                    username: username
                } );
        
                newProfile.save().then( savedProfile => {
                    console.log( savedProfile );
                    res.send( { message: 'Your new profile has been registered successfully.' } )
                } ).catch( save_err => {
                    console.log( 'profile save error: ', save_err )
                } );                
            }

        } else {
            res.send({ message: 'User with this email has registered already.' })
        }
    });

    //res.send(' I got it, bitch ');
}