var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken')
import { Response, Request, NextFunction } from 'express'
import Profile from '../../model/profile'


export async function login( req: Request, res: Response ) {
    const loginInfo = req.body;

    Profile.findOne({
        email: loginInfo.email
    }).then( userInfo => {
        //console.log( 'userInfo', userInfo )
        if( userInfo ){
            bcrypt.compare( loginInfo.password, userInfo.password, function( err, result ){
                if( err ){
                    console.log( 'bcrypt compare error', err ); 
                }
                if( result ) {
                    const payload = {
                        iss: 'http://localhost:4000',
                        email: userInfo.email,
                        username: userInfo.username
                    }

                    const token = jwt.sign( payload, 'devel-jd', { expiresIn: 60 * 120 } )

                    res.json( { success: true, message: 'Login Successful', token: token } )
                }else{
                    res.json( { success: false, message: 'User/password is not matched' } )        
                }
            } )
        }else{
            res.json( { success: false, message: 'User/password is not matched' } )
        }
    } ).catch( err => {
        console.log( err )
    } ) 
}